/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.artin.javaakademie.academyhomework.ejb;

import cz.artin.javaakademie.academyhomework.model.Car;
import java.util.List;

/**
 *
 * @author tpoledny
 */
public interface CarService {
    
    List<Car> getAllCars();
    
    Car getCar(int id);
    
    void save(Car car);
    
}
