/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.artin.javaakademie.academyhomework.jsf;

import javax.inject.Inject;
import javax.inject.Named;
import cz.artin.javaakademie.academyhomework.ejb.CarService;
import cz.artin.javaakademie.academyhomework.model.Car;
import java.util.List;

/**
 *
 * @author tpoledny
 */
@Named
public class AllCars {
    
    @Inject
    private CarService carsService;


    public List<Car> getCars() {
        return carsService.getAllCars();
    }
    
}
