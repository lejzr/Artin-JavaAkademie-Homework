package cz.artin.javaakademie.academyhomework.jsf.validator;

import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

/**
 *
 * @author Tomas Poledny 
 */
@Named
@FacesValidator("cz.artin.javaakademie.academyhomework.jsf.validator.YearValidator")
public class YearValidator implements Validator{

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        Integer input = (Integer) value;
        
        if((input < 1900) || (input > 2100)){
        	throw new ValidatorException(new FacesMessage("Year must be between 1900 and 2100"));
        }
        
    }
    
}