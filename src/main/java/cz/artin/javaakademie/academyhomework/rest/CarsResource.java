package cz.artin.javaakademie.academyhomework.rest;

import java.net.URI;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import cz.artin.javaakademie.academyhomework.ejb.CarService;
import cz.artin.javaakademie.academyhomework.model.Car;


/**
 *
 * @author tomas.poledny@artin.cz
 */
@Path("/cars")
public class CarsResource {

    @Context
    private UriInfo uri;

    @Inject
    private CarService carService;

    @Path("/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Car> getAllCars() {
        return carService.getAllCars();
    }

    @Path("/")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addCar(Car car) {
        carService.save(car);
        return Response.created(URI.create(uri.getPath() + "/" + car.getId())).build();
    }
    
    @Path("/{ID}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Car getCarById(@PathParam("ID") int id){
		return carService.getCar(id);
    }
}
