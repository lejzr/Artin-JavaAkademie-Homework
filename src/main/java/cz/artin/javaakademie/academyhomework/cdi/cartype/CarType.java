package cz.artin.javaakademie.academyhomework.cdi.cartype;

/**
 *
 * @author Tomas Poledny <tomas.poledny at artin.cz>
 */
public interface CarType {

    String getName();

    int getVersion();

}
