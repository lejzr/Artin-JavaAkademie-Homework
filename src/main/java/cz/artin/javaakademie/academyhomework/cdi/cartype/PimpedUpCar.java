package cz.artin.javaakademie.academyhomework.cdi.cartype;

import javax.inject.Named;

import cz.artin.javaakademie.academyhomework.cdi.qualifier.PimpedUpCarQualifier;

@Named
@PimpedUpCarQualifier
public class PimpedUpCar implements CarType  {

	@Override
	public String getName() {
		return "PimpedUpCar";
	}

	@Override
	public int getVersion() {
		return 1;
	}
	
	@Override
	public String toString() {
        return getName() + " : " + getVersion();
    }

}
