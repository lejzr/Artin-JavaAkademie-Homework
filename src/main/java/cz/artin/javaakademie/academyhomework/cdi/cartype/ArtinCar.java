package cz.artin.javaakademie.academyhomework.cdi.cartype;

import javax.inject.Named;

import cz.artin.javaakademie.academyhomework.cdi.qualifier.ArtinCarQualifier;

/**
 *
 * Typ CDI beany musi byt "ArtinCarQualifier"
 */
@Named
@ArtinCarQualifier
public class ArtinCar implements CarType{

    @Override
    public String getName() {
        return "ArtinCar";
    }

    @Override
    public int getVersion() {
        return 1;
    }

    @Override
    public String toString() {
        return getName() + " : " + getVersion();
    }

    
}
