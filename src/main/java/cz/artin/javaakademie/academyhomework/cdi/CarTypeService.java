package cz.artin.javaakademie.academyhomework.cdi;

import javax.inject.Inject;
import javax.inject.Named;

import cz.artin.javaakademie.academyhomework.cdi.cartype.CarType;
import cz.artin.javaakademie.academyhomework.cdi.qualifier.ArtinCarQualifier;
import cz.artin.javaakademie.academyhomework.cdi.qualifier.PimpedUpCarQualifier;

/**
 * Vlozte vytvoreny CarType do fieldu otherCar
 */
@Named
public class CarTypeService {
    
    
    @Inject
    @ArtinCarQualifier
    private CarType artinCar;
    
    @Inject
    @PimpedUpCarQualifier
    private CarType otherCar;
    

    public CarType getArtinCar(){
        return artinCar;
    }
    
    public CarType getOtherCar() {
        return otherCar;
    }
    
}
